from member.models import Member
from django.contrib.auth.models import User
from django import forms


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username','email', 'password')

class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ('roll_no','name','address','branch','post','mobile_no','linkedin_link','year','facebook_link','twitter_link','instagram_link')





