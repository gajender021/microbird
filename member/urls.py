from	 django.conf.urls import patterns, url, include
# import views
from member import views

urlpatterns = [

url(r'^create-member/$',views.newmember,name="create-member"),
url(r'^all-member/$',views.viewAllMember,name="all-member"),
url(r'^current-member/$',views.view_current_member,name="current-member"),
url(r'^alumni-member/$',views.view_alumni_member,name="alumni-member"),
url(r'^achievement/$',views.achievement,name="achievement"),
url(r'^login/$', views.login_user, name='login'),
url(r'^success/$', views.success, name='success'),
url(r'^logout/$', views.logout_user, name='logout'),









]