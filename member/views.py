from django.shortcuts import render
# import requests
import operator
from django.http import JsonResponse, HttpResponse
from django.http import HttpResponseRedirect
# Create your views here.
from .models import *
# from datetime import datetime
from django.shortcuts import render
from member.forms import UserForm, MemberForm
from django.template import RequestContext
from django.shortcuts import render_to_response ,redirect
from django.contrib.auth import authenticate, login ,logout
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_exempt

@login_required
@csrf_exempt		
def newmember(request):
	context = RequestContext(request)
	registered = False
	print request.method
	if request.method == 'POST':
		myfile = request.FILES.get('myfile')
		user_form = UserForm(data=request.POST)
		profile_form = MemberForm(data=request.POST)
		if user_form.is_valid() and profile_form.is_valid():
			user = user_form.save()
			user.set_password(user.password)
			user.save()

			profile = profile_form.save(commit=False)
			profile.profile_pic = myfile
			profile.user = user
			profile.date=datetime.now()

			profile.save()
			registered = True

		else:
			print user_form.errors, profile_form.errors
	else:
		user_form = UserForm()
		profile_form = MemberForm()

	
	return render_to_response(
            'member/newmember.htm',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered},
            context)

@login_required
def viewAllMember(request):
	print request.method
	objectMember =  Member.objects.all()
	context={
	'obj':objectMember
	}
	return render (request ,'member/viewmember.htm',context)

def view_current_member(request):
	print request.method
	objectMember1=Member.objects.filter(is_active=True).filter(is_member=True)
	oo1 =sorted(objectMember1, key =operator.attrgetter('year'))
	m=[]
	if(oo1):
		i=0
		
		b=[]
		while i<len(oo1)-1:
		        b.append(oo1[i])
		        if oo1[i].year!=oo1[i+1].year:
		                m.append(b)
		                b=[]
		        i=i+1
		b.append(oo1[i])
		m.append(b)


	context={
	'obj1':m
	}
	return render (request,'member/view-current-member.htm',context)


def view_alumni_member(request):
	print request.method
	objectMember33=Member.objects.filter(is_active=False).filter(is_member=True)#.order_by('year')
	oo =sorted(objectMember33, key =operator.attrgetter('year'))
	n=[]
	if(oo):
		i=0
		b=[]
		while i<len(oo)-1:
		        b.append(oo[i])
		        if oo[i].year!=oo[i+1].year:
		                n.append(b)
		                b=[]
		        i=i+1
		b.append(oo[i])
		n.append(b)




	context={
	'obj2':n,
	}
	return render (request,'member/view-alumni-member.htm',context)

def achievement(request):
    return render_to_response('member/achievement.html')


@csrf_exempt
def login_user(request):
	context = RequestContext(request)
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(username=username, password=password)
		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect('/member/success/')
			else:
				return HttpResponse("Your account is disabled.")
		else:
			print "Invalid login details: {0}, {1}".format(username, password)
			return HttpResponse("Invalid login details supplied.")
	else:
		 return render_to_response('member/login.html', {}, context)
@login_required
def success(request):
	return render(request,'member/success.html')

def logout_user(request):
	logout(request)
	return render_to_response('member/login.html')