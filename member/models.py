from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Member(models.Model):
	user=models.OneToOneField(User)

	roll_no=models.CharField(max_length=50,blank=True,null=True)
	name=models.CharField(max_length=50,blank=True,null=True)
	address=models.CharField(max_length=100,blank=True,null=True)
	branch=models.CharField(max_length=100,blank=True,null=True)
	profile_pic=models.ImageField(upload_to='image/',blank=True,null=True)
	mobile_no=models.IntegerField(default=True,blank=True,null=True)
	linkedin_link=models.CharField(max_length=300,blank=True,null=True)
	facebook_link=models.CharField(max_length=300,blank=True,null=True)
	twitter_link=models.CharField(max_length=300,blank=True,null=True)
	instagram_link=models.CharField(max_length=300,blank=True,null=True)
	year=models.CharField(max_length=20,default=1,blank=True,null=True)
	post=models.CharField(max_length=50,blank=True,null=True)
	is_active=models.BooleanField(default=True)
	is_member=models.BooleanField(default=True)
	date=models.DateTimeField(null=True)

	def __str__(self):
		return self.user.username+'--------'+self.name
		
	