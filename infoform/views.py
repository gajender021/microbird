from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from .models import *
# Create your views here.
from django.contrib.auth.decorators import login_required



def infoform(request):
	if request.method=="POST":
		event_id1=request.POST.get('eventid')
		event_title1=request.POST.get('eventTitle')
		rollno1=request.POST.get('rollno')
		name1=request.POST.get('name')
		mobileno1=request.POST.get('mobileno')
		branch1=request.POST.get('branch')
		emailid1=request.POST.get('emailid')
		year1=request.POST.get('year')

		infoObj=Infoform.objects.create()
		
		infoObj.event_no=event_id1
		infoObj.event_title=event_title1
		infoObj.roll_no=rollno1
		infoObj.name=name1
		infoObj.mobile_no=mobileno1
		infoObj.branch=branch1
		infoObj.email_id=emailid1
		infoObj.year=year1

		infoObj.save()

		return JsonResponse("successfully",safe=False)
	else:
		event_id=request.GET.get('id')
		event_title=request.GET.get('eventTitle')

		context={
	'event_id':event_id,
	'event_title':event_title
	}
		return render(request,'infoform/infoform.htm',context)

@login_required
def view_all_form(request,num):
	print request.method
	viewobj=Infoform.objects.filter(event_no=num)
	context={
	'view1':viewobj
	}
	return render (request,'infoform/viewform.htm',context)