from __future__ import unicode_literals

from django.db import models

# Create your models here.
class  Infoform(models.Model):
	event_no=models.IntegerField(null=True, blank=True)
	event_title=models.CharField(max_length=50,blank=True,null=True)
	roll_no=models.CharField(max_length=50,blank=True,null=True)
	name=models.CharField(max_length=50,blank=True,null=True)
	mobile_no=models.IntegerField(default=1,blank=True,null=True)
	branch=models.CharField(max_length=50,blank=True,null=True)
	email_id=models.CharField(max_length=50,blank=True,null=True)
	year=models.IntegerField(default=1,blank=True,null=True)

	def __int__(self):
		return self.id