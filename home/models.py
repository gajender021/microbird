from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Message(models.Model):
	name=models.CharField(max_length=50,blank=True,null=True)
	subject=models.CharField(max_length=50,blank=True,null=True)

	email=models.CharField(max_length=50,blank=True,null=True)
	query=models.TextField(blank=True,null=True)
	is_reply=models.BooleanField(default=False)
	date=models.DateTimeField(null=True)
	
	def __str__(self):
		return self.email

class Subscribe(models.Model):
	email=models.CharField(max_length=50,blank=True,null=True)


	def __str__(self):
		return self.email