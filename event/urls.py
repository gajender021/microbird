from   django.conf.urls import patterns, url, include

from event import views

urlpatterns=[

url(r'^create-event/$',views.newevent,name="create-event"),
url(r'^all-event/$',views.viewAllevent,name="all-event"),
url(r'^all-upcoming-event/$',views.viewupcomingevent,name="all-upcoming-event"),
url(r'^full-info-event/$',views.viewfullinfoevent,name="full-info-event")



]