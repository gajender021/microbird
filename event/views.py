from django.shortcuts import render
from django.http  import JsonResponse ,HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
@login_required
@csrf_exempt
def newevent(request):
	if request.method=="POST":
		title2=request.POST.get('title1')
		description2=request.POST.get('description1')
		guider3=request.POST.get('guider1')
		location2=request.POST.get('location1')
		date2=request.POST.get('date1')
		time2=request.POST.get('time1')
		
		eventObj=Event.objects.create()

		eventObj.title=title2
		eventObj.description=description2
		eventObj.guider=guider3
		eventObj.location=location2
		eventObj.is_active=True
		eventObj.date=date2
		eventObj.time=time2
		

		eventObj.save()

		return JsonResponse("successfull",safe=False)
	else:
		return render(request,'event/newevent.htm')

@login_required
def viewAllevent(request):
	objectEvent=Event.objects.all()
	context={
	'obj':objectEvent
		}
	return	render	(request,'event/viewevent.htm',context)

def viewupcomingevent(request):
	objectEvent1=Event.objects.filter(is_active=True)
	context={
	'obj1':objectEvent1
		}
	return	render	(request,'event/view-upcoming-event.htm',context)

def viewfullinfoevent(request):
	event_id=request.GET.get('eventid')
	if event_id =='':
		return HttpResponse("404")
	objectEvent2=Event.objects.get(id=event_id)
	context={
	'obj2':objectEvent2
		}
	return	render	(request,'event/view-full-info-event.htm',context)
