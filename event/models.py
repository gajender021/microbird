from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Event(models.Model):
	title=models.CharField(max_length=100,blank=True,null=True)
	description=models.TextField(blank=True,null=True)
	guider=models.CharField(max_length=50,blank=True,null=True)
	location=models.CharField(max_length=50,blank=True,null=True)
	date=models.DateField(blank=True,null=True)
	time=models.TimeField(blank=True,null=True)
	is_active=models.BooleanField(default=True)
	
	def __str__(self):
		return self.title